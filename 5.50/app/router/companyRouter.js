//Khai báo thư viện express
const express = require("express");
const { companyMiddleware } = require('../middleware/companyMiddleware');

//Tạo Router
const companyRouter = express.Router();

//Sử dụng middleware
companyRouter.use(companyMiddleware);

companyRouter.get('/company', (request, response) => {
    console.log("Get all company");
    response.json({
        message: 'Get All Company',
    })
});

//get a company
companyRouter.get('/company/:companyCode', (request, response) => {
    let id = request.params.companyCode;

    console.log(`Get all companyCode = ${id}`);
    response.json({
        message: `Get all companyCode = ${id}`
    })
});

//create a company
companyRouter.post('/company', (request, response) => {
    let body = request.body;

    console.log("Create a company");
    console.log(body);
    response.json({
        ...body
    })
});

//update a company
companyRouter.put('/company/:companyCode', (request, response) => {
    let id = request.params.companyCode;
    let body = request.body;

    console.log("Update a company");
    console.log({ id, ...body });
    response.json({
        message: { id, ...body }
    })
});

//delete a company
companyRouter.delete('/company/:companyCode', (request, response) => {
    let id = request.params.companyCode;

    console.log(`delete a company ${id}`);
    response.json({
        message: `delete a company ${id}`
    })
});

module.exports = { companyRouter };