//Khai báo thư viện express
const express = require('express');
const { companyRouter } = require('./app/router/companyRouter');

//Khai báo app
const app = express();

//Khai báo cổng nodejs
const port = 8000;

//sử dụng unicode
app.use(express.urlencoded({
    urlencoded: true
}));

//Khai báo API
const { Company } = require("./company.js");

var company_1 = new Company('1', 'Alfreds Futterkiste', 'Maria Anders', 'Germany');
var company_2 = new Company('2', 'Centro comercial Moctezuma', 'Francisco Chang', 'Mexico');
var company_3 = new Company('3', 'Ernst Handel', 'Roland Mendel', 'Austria');
var company_4 = new Company('4', 'Island Trading', 'Helen Bennett', 'UK');
var company_5 = new Company('5', 'Laughing Bacchus Winecellars', 'Yoshi Tannamuri', 'Canada');
var company_6 = new Company('6', 'Magazzini Alimentari Riuniti', 'Giovanni Rovelli', 'Italy');

var allCompanies = [company_1, company_2, company_3, company_4, company_5, company_6]


//Khai báo API
app.get('/companies', (request, response) => {
    console.log("Get all companies");
    response.json({
        companies: allCompanies
    })
});

app.get('/companies/:companyId', (request, response) => {
    let companyId = request.params.companyId;

    allCompanies.filter(paramID => {
        if (companyId == paramID.getCompanyId()) {
            response.status(200).json({
                Company: paramID.getCompany()
            })
        }
    });

    allCompanies.filter(paramID => {
        if (companyId !== paramID.getCompanyId()) {
            response.status(403).json({
                Company: "Không tồn tại Company này!!!"
            })
        }
    });

})

//sử dụng router
app.use('/', companyRouter);

//Chạy nodejs trên cổng
app.listen(port, () => {
    console.log(`App listening port ${port}`);
});