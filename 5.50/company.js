class Company {
    constructor(id, company, contact, country) {
        this.id = id;
        this.company = company;
        this.contact = contact;
        this.country = country;
    }
    getCompany() {
        return {
            "id": this.id,
            "name": this.company,
            "contact": this.contact,
            "country": this.country
        }
    }
    getCompanyId() {
        return this.id
    }
}
module.exports = { Company }