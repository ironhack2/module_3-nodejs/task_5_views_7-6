const express = require('express');

const path = require('path');

const app = express();

const port = 8000;

app.use(express.static(__dirname + `/views`));

// Cấu hình để app được được tiếng Việt UTF8
app.use(express.urlencoded({
    extended: true
}))

//Khai báo API
app.get("/", (request, response) => {
    response.send(`<h3>Hãy thêm:  /pizza365,    để vào trang Pizza365 </h3>`)
});

app.get("/pizza365", (request, response) => {
    console.log(__dirname);
    response.sendFile(path.join(__dirname + '/views/pizza365.html'));
});

//Chạy app express
app.listen(port, () => {
    console.log("App listening on port 8000" + port)
})