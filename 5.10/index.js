const express = require('express');

const app = express();

//Khai báo cổng
const port = 8000;

//Đọc body request dạng json
app.use(express.json());

// Cấu hình để app được được tiếng Việt UTF8
app.use(express.urlencoded({
    extended: true
}))

//Khai báo API
app.get("/", (request, response) => {
    let today = new Date();
    response.status(200).json(`Xin chào bạn!!! Hôm nay là ngày ${today.getDate()} tháng ${today.getMonth()+1} năm ${today.getFullYear()}`)
});

//Chạy app express
app.listen(port, () => {
    console.log("App listening on port 8000")
})