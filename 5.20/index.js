const express = require('express');

const path = require('path');

const app = express();

//Khai báo cổng
const port = 8000;

//Đọc body request dạng json
app.use(express.json());

// Cấu hình để app được được tiếng Việt UTF8
app.use(express.urlencoded({
    extended: true
}))

//Khai báo API
app.get("/", (request, response) => {
    console.log(__dirname);
    response.sendFile(path.join(__dirname + '/views/index.html'));
});

app.get("/about", (request, response) => {
    console.log(__dirname);
    response.sendFile(path.join(__dirname + '/views/about.html'));
});

app.get("/sitemap", (request, response) => {
    console.log(__dirname);
    response.sendFile(path.join(__dirname + '/views/sitemap.html'));
});

//Chạy app express
app.listen(port, () => {
    console.log("App listening on port 8000" + port)
})