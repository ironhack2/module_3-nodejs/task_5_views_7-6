//Khai báo thư viện express
const express = require('express');
const path = require('path');

//Khai báo app
const app = express();

//Khai báo cổng nodejs
const post = 8000;

//sử dụng unicode
app.use(express.urlencoded({
    urlencoded: true
}));

//Khai báo API
app.get('/', (request, response) => {

    response.send("hello world")
})

app.get('/example-call', (request, response) => {
    response.sendFile(path.join(__dirname + '/views/example-call.html'))
})

//Chạy trên cổng nodeJS
app.listen(post, () => {
    console.log(`App chạy trên cổng ${post}`);
});